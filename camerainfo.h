#ifndef CAMERAINFO_H
#define CAMERAINFO_H

#include <QList>
#include <QSize>
#include <QCameraInfo>
#include <QCameraImageCapture>

class CameraInfo
{
public:
    CameraInfo(const QCameraInfo& cameraInfo);

    static CameraInfo* getInstance(const QCameraInfo& cameraInfo)
    {
        CameraInfo* c = new CameraInfo(cameraInfo);
        c->setSupportedResolutions();
        return c;
    }

    QCameraInfo getQCameraInfo() const;
    QList<QSize> getSupportedResolutions();

private:

    void setSupportedResolutions();

private:
    QList<QSize> supportedResolution;
    QCameraInfo cameraInfo;
};

#endif // CAMERAINFO_H
