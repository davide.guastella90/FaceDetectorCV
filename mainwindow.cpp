#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    detector = nullptr;
    saveNextDetection = false;
    pen = QPen(QColor(255, 34, 255, 255));
    eyePen = QPen(QColor(17, 205, 220, 255));
    pen.setWidth(5);
    eyePen.setWidth(5);
    ui->setupUi(this);

    ui->facesOutputFolderLineEdit->setText(QDir::homePath() + QDir::separator() + "Output");


    //only in development stage, to remove
    featuresFileName = "/home/davide/HaarClassifierQT/HaarClassifier/Definitive/haarcascade_frontalface_alt2.xml";


}

MainWindow::~MainWindow()
{
    grabber->stopGrabbing();
    grabber->terminate();
    grabber->wait();

    if(detector != nullptr)
    {
        detector->terminate();
        detector->wait();
    }
    delete ui;
}

void MainWindow::onFrameGrabbed(const QImage image)
{
    if(!image.isNull())
    {
        Mat matFrame = ImageUtils::QImage2Mat(image);
        if(detector != nullptr)
        {
            detector->setFrame(matFrame);
            eyesDetector->setFrame(matFrame);
        }
        QImage scaledImage = image.scaled(ui->webcamFrameLabel->size(),
                                          Qt::KeepAspectRatio,
                                          Qt::SmoothTransformation);

        QPixmap pix = QPixmap::fromImage(scaledImage);
        QPainter *paint = new QPainter(&pix);
        paint->setPen(pen);
        //--------------------------------------------

        if(this->faces.size() > 0)
        {
            mutex.lock();
            currentFaces = faces.dequeue();
            mutex.unlock();
        }

        if(!currentFaces.empty())
        {

            int faceIdx = 0;
            for(const auto& face : currentFaces)
            {
                paint->drawRect(face);

                if(saveNextDetection)
                {
                    QString timestamp = QDateTime::currentDateTime().toString("HH'h'mm'm'ss's_'");
                    timestamp.append(QString::number(faceIdx++));
                    QString fname = ui->facesOutputFolderLineEdit->text() + QDir::separator() + timestamp + ".png";
                    pix.copy(face).save(fname, "PNG");

                }
            }
        }
        //--------------------------------------------

        //--------------------------------------------
        if(this->eyes.size() > 0)
        {
            eyesMutex.lock();
            currentEyes = eyes.dequeue();
            eyesMutex.unlock();
        }

        if(!currentEyes.empty())
        {
            paint->setPen(eyePen);
            for(const auto& eye : currentEyes)
            {
                paint->drawRect(eye);

            }
        }
        //--------------------------------------------
        if(saveNextDetection)
        {
            int faceIdx = 0;
            for (int i=0;i<std::min(currentFaces.size(),currentEyes.size());i++)
            {

                    QString timestamp = QDateTime::currentDateTime().toString("HH'h'mm'm'ss's_'");
                    timestamp.append(QString::number(faceIdx++));
                    QString fname = ui->facesOutputFolderLineEdit->text() + QDir::separator() + timestamp + ".png";


                    //static Mat PreprocessFaceImage(const Mat& frame, const Rect& faceRect, const Rect& leftEyeRect,const Rect& rightEyeRect)
                    //pix.copy(face).save(fname, "PNG");
                    if(currentEyes.size() % 2 == 0)
                    {
                        Rect faceRect = cv::Rect(currentFaces.at(i).x(),currentFaces.at(i).y(),currentFaces.at(i).width(),currentFaces.at(i).height());
                        Rect lRect = cv::Rect(currentEyes.at(i).x(),currentEyes.at(i).y(),currentEyes.at(i).width(),currentEyes.at(i).height());
                        Rect rRect = cv::Rect(currentEyes.at(i+1).x(),currentEyes.at(i+1).y(),currentEyes.at(i+1).width(),currentEyes.at(i+1).height());
                        Mat face = FaceDetectionUtils::PreprocessFaceImage(matFrame,faceRect,lRect,rRect);

                        //cv::imwrite(fname.toStdString(),face);
                        //QPixmap::fromImage(ImageUtils::Mat2QImage(face)).save(fname, "PNG");
                    }
            }
        }


        saveNextDetection = false;
        delete paint;

        ui->webcamFrameLabel->setPixmap(pix);
    }
}

void MainWindow::on_refreshDeviceListPushButton_clicked()
{
    QList<QCameraInfo> devices = QCameraInfo::availableCameras();

    foreach(CameraInfo* p, cameraList)
    {
        delete p;
    }

    cameraList.clear();
    ui->deviceListComboBox->clear();
    ui->resolutionsListWidgets->clear();
    for(const auto& device : devices)
    {
        CameraInfo* i = CameraInfo::getInstance(device);
        cameraList.append(i);
        ui->deviceListComboBox->addItem(i->getQCameraInfo().description());
    }

    if(cameraList.size() > 0)
    {
        selectedCamera = cameraList.at(0);
        for(const auto& res : selectedCamera->getSupportedResolutions())
        {
            ui->resolutionsListWidgets->addItem(QString(QString::number(res.width())).append("x").append(QString::number(res.height())));
        }
    }
}

void MainWindow::on_deviceListComboBox_currentIndexChanged(int index)
{
    selectedCamera = cameraList.at(index);
    ui->resolutionsListWidgets->clear();
    for(const auto& res : selectedCamera->getSupportedResolutions())
    {
        ui->resolutionsListWidgets->addItem(QString(QString::number(res.width())).append("x").append(QString::number(res.height())));
    }
    ui->statusBar->showMessage(QString("Selected webcam ").append(selectedCamera->getQCameraInfo().deviceName()), 5000);
}

void on_selectVideoPushButton_clicked()
{
}

void MainWindow::on_startCameraPushButton_clicked()
{
    grabber = QSharedPointer<WebcamGrabber>(new WebcamGrabber(0));
    connect(grabber.data(), &WebcamGrabber::grabbedFrame, this, &MainWindow::onFrameGrabbed);
    grabber->start();

}

void MainWindow::on_resolutionsListWidgets_itemClicked(QListWidgetItem *item)
{
    QList<QString> res = item->text().split('x');
    grabber->setResolution(QSize(res.at(0).toInt(), res.at(1).toInt()));
}

void MainWindow::on_loadFaceDetectorFeaturesPushButton_clicked()
{
    featuresFileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select file"), QDir::homePath() , "*.*");

    ui->faceFeaturesFileLineEdit->setText(featuresFileName);

}

void MainWindow::on_startFaceDetectionPushButton_clicked()
{
    //detector = new FeaturesDetector();
    //detector->LoadClassifier(featuresFileName);

    detector = FeaturesDetector::get("faceDetector",featuresFileName, QColor(255, 34, 255, 255));
    connect(detector, &FeaturesDetector::detected, [this](const QList<QRect> detections)
    {
        mutex.lock();
        this->faces.enqueue(detections);
        mutex.unlock();
    });

    detector->start();


    startEyesDetector();
}

void MainWindow::startEyesDetector()
{
    QString eyesFeaturesFname("/home/davide/HaarClassifierQT/HaarClassifier/Definitive/haarcascade_eye_tree_eyeglasses.xml");
    detector = FeaturesDetector::get("eyesDetector",eyesFeaturesFname, QColor(17, 205, 220, 255));
    connect(eyesDetector, &FeaturesDetector::detected, [this](const QList<QRect> detections)
    {
        eyesMutex.lock();
        this->eyes.enqueue(detections);
        eyesMutex.unlock();
    });

    eyesDetector->start();
}

void MainWindow::on_stopFaceDetectionPushButton_clicked()
{
    if(detector == nullptr)
    {
        return;
    }

    detector->terminate();
    detector->wait();
}

void MainWindow::on_saveDetectionPushButton_clicked()
{
    saveNextDetection = true;
}

void MainWindow::on_addDetector(const QString &name, const QString &featureFname, const QColor &color, bool activateNow)
{
    //FeaturesDetector* detector = FeaturesDetector::get(name,featureFname,color);

    QListWidgetItem* item = new QListWidgetItem(name, ui->detectorsListWidget);
    item->setFlags(item->flags() | Qt::ItemIsUserCheckable); // set checkable flag
    item->setCheckState(activateNow ? Qt::Checked : Qt::Unchecked); // AND initialize check state
}

void MainWindow::on_addDetectorPushButton_clicked()
{
    AddDetectorDialog* d = new AddDetectorDialog();
    connect(d,&AddDetectorDialog::addDetector,this,&MainWindow::on_addDetector);
    d->show();
}
