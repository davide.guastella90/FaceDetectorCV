#ifndef IMAGEUTILS_H
#define IMAGEUTILS_H

#include <QPixmap>
#include <QImage>
#include <QRect>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

class ImageUtils
{
public:
    static QImage Mat2QImage(cv::Mat const& src)
    {
        cv::Mat temp; // make the same cv::Mat
        cvtColor(src, temp, CV_BGR2RGB); // cvtColor Makes a copt, that what i need
        QImage dest((const uchar *) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
        dest.bits(); // enforce deep copy, see documentation
        // of QImage::QImage ( const uchar * data, int width, int height, Format format )
        return dest;
    }

    static cv::Mat QImage2Mat(QImage const& src)
    {
        cv::Mat tmp(src.height(), src.width(), CV_8UC3, (uchar*)src.bits(), src.bytesPerLine());
        cv::Mat result; // deep copy just in case (my lack of knowledge with open cv)
        cvtColor(tmp, result, CV_BGR2RGB);
        return result;
    }

    static void ConvertToGrayscale(Mat* img)
    {
        if(img->channels() == 3)
            cvtColor(*img, *img, CV_BGR2GRAY);
        else if(img->channels() == 4)
            cvtColor(*img, *img, CV_BGRA2GRAY);
    }

    //mode:
    //   false  -> draw circles
    //   true   -> draw rectangles
    static void DrawContours(Mat* Image, QRect contoursRect, bool drawRectangle)
    {
        if(contoursRect.isEmpty())
            return;

        if(!drawRectangle)
        {
            int radius = cvRound((contoursRect.width() + contoursRect.height()) * 0.25);
            circle(*Image, cv::Point(contoursRect.center().x(), contoursRect.center().y()), radius, Scalar(0, 0, 0), 4, 8, 0);
        }
        else
        {
            rectangle(*Image,
                      cvPoint(contoursRect.x(), contoursRect.y()),
                      cvPoint(contoursRect.x() + contoursRect.width(), contoursRect.y() + contoursRect.height()),
                      cvScalar(0, 255, 255), 3, 8, 0);
        }

    }



};

#endif // IMAGEUTILS_H
