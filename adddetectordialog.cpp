#include "adddetectordialog.h"
#include "ui_adddetectordialog.h"

AddDetectorDialog::AddDetectorDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddDetectorDialog)
{
    ui->setupUi(this);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &AddDetectorDialog::on_OkButtonClicked);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &AddDetectorDialog::on_CancelButtonClicked);
    color = QColor(0,0,0);
    ui->featuresColorPushButton->setStyleSheet("background-color: rgb(0,0,0);");
}

void AddDetectorDialog::on_OkButtonClicked()
{
    emit addDetector(ui->detectorNameLineEdit->text(), ui->featuresFileNamelineEdit->text(), color, ui->activateNowCheckBox->isChecked());
    close();
}

void AddDetectorDialog::on_CancelButtonClicked()
{
    close();
}

AddDetectorDialog::~AddDetectorDialog()
{
    delete ui;
}

void AddDetectorDialog::on_featuresColorPushButton_clicked()
{
    color = QColorDialog::getColor(color);

    QString css = "background-color: rgb(";
    css.append(QString::number(color.red())).append(",");
    css.append(QString::number(color.green())).append(",");
    css.append(QString::number(color.blue())).append(");");
    ui->featuresColorPushButton->setStyleSheet(css);
}
