#-------------------------------------------------
#
# Project created by QtCreator 2017-04-14T18:38:10
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WebcamGrabber

TEMPLATE = app

LIBS += -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml
LIBS += -lopencv_imgcodecs -lopencv_video -lopencv_features2d
LIBS += -lopencv_objdetect -lopencv_flann -lopencv_calib3d -lopencv_videoio

SOURCES += main.cpp\
        mainwindow.cpp \
    webcamgrabber.cpp \
    camerainfo.cpp \
    cannydetector.cpp \
    Detector/featuresdetector.cpp \
    adddetectordialog.cpp

HEADERS  += mainwindow.h \
    imageutils.h \
    webcamgrabber.h \
    camerainfo.h \
    cannydetector.h \
    Detector/featuresdetector.h \
    Detector/facedetectionutils.h \
    adddetectordialog.h

FORMS    += mainwindow.ui \
    adddetectordialog.ui
