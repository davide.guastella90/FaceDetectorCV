#ifndef FACEDETECTOR_H
#define FACEDETECTOR_H

#include <QThread>
#include <QMutex>
#include <QRect>
#include <iostream>
#include <vector>
#include <QColor>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"

using namespace cv;
using namespace std;

class FeaturesDetector : public QThread
{
    Q_OBJECT

private:
    FeaturesDetector();
    FeaturesDetector(const QString &name, const QString &featureFname, const QColor &color);
public:
    void run() Q_DECL_OVERRIDE;

    void setFrame(const Mat frame);

    QString getName() const;
    QString getFeaturesFileName() const;
    QColor getColor() const;

    static FeaturesDetector* get(const QString &name, const QString &featureFname, const QColor &color)
    {
        FeaturesDetector* detector = new FeaturesDetector(name,featureFname,color);
        detector->LoadClassifier(featureFname);
        return detector;
    }

private:
    CascadeClassifier classifier;

    //Search for many faces.
    int flags;

    //Smallest face size.
    Size minFeatureSize;

    //How many sizes to search.
    float searchScaleFactor;

    // Reliability vs many faces.
    int minNeighbors;

    QMutex mutex;

    //the name of this detector
    QString detectorName;

    //used to highlight the features this detector finds
    QColor color;

    //the filename of the features (xml)
    QString featureFileName;

    //the current frame this detector works on
    Mat frame;

    void LoadClassifier(QString filename);

signals:
    void detected(const QList<QRect> detections);

public slots:
};

#endif // FACEDETECTOR_H
