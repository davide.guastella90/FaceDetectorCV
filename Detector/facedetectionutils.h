#ifndef FACEDETECTIONUTILS_H
#define FACEDETECTIONUTILS_H

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "../imageutils.h"

using namespace cv;

class FaceDetectionUtils
{

private:

    //scale, rotate, and translate the image so that the eyes are aligned, followed by the removal of the forehead, chin, ears, and background from the face image.
    static Mat applyGeometricalTransformation(Mat* image, const Point &leftEye, const Point& rightEye)
    {
        // Since we found both eyes, lets rotate & scale & translate the face so that the 2 eyes
        // line up perfectly with ideal eye positions. This makes sure that eyes will be horizontal,
        // and not too far left or right of the face, etc.
        const double DESIRED_LEFT_EYE_X   = 0.16;
        const double DESIRED_LEFT_EYE_Y   = 0.14;
        const int DESIRED_FACE_WIDTH      = 70;
        const int DESIRED_FACE_HEIGHT     = 70;

        // Get the center between the 2 eyes.
        Point2f eyesCenter = Point2f((leftEye.x + rightEye.x) * 0.5f, (leftEye.y + rightEye.y) * 0.5f);

        // Get the angle between the 2 eyes.
        double dy = (rightEye.y - leftEye.y);
        double dx = (rightEye.x - leftEye.x);
        double len = sqrt(dx * dx + dy * dy);

        // Convert from radians to degrees.
        double angle = atan2(dy, dx) * 180.0 / CV_PI;

        // Hand measurements shown that the left eye center should ideally be at roughly (0.19, 0.14) of a scaled face image.
        const double DESIRED_RIGHT_EYE_X = (1.0f - DESIRED_LEFT_EYE_X);

        // Get the amount we need to scale the image to be the desired fixed size we want.
        double desiredLen = (DESIRED_RIGHT_EYE_X - DESIRED_LEFT_EYE_X) * DESIRED_FACE_WIDTH;
        double scale = desiredLen / len;

        // Get the transformation matrix for rotating and scaling the face to the desired angle & size.
        Mat rot_mat = getRotationMatrix2D(eyesCenter, angle, scale);

        // Shift the center of the eyes to be the desired center between the eyes.
        rot_mat.at<double>(0, 2) += DESIRED_FACE_WIDTH * 0.5f - eyesCenter.x;
        rot_mat.at<double>(1, 2) += DESIRED_FACE_HEIGHT * DESIRED_LEFT_EYE_Y - eyesCenter.y;

        // Rotate and scale and translate the image to the desired angle & size & position!
        // Note that we use 'w' for the height instead of 'h', because the input face has 1:1 aspect ratio.

        // Clear the output image to a default grey.
        Mat warped = Mat(DESIRED_FACE_HEIGHT + 30, DESIRED_FACE_WIDTH + 30, CV_8U, Scalar(128));
        warpAffine(*image, warped, rot_mat, warped.size());
        return warped;
    }



    // Histogram Equalize seperately for the left and right sides of the face.
    static void equalizeLeftAndRightHalves(Mat* faceImg)
    {
        // It is common that there is stronger light from one half of the face than the other. In that case,
        // if you simply did histogram equalization on the whole face then it would make one half dark and
        // one half bright. So we will do histogram equalization separately on each face half, so they will
        // both look similar on average. But this would cause a sharp edge in the middle of the face, because
        // the left half and right half would be suddenly different. So we also histogram equalize the whole
        // image, and in the middle part we blend the 3 images together for a smooth brightness transition.

        int w = faceImg->cols;
        int h = faceImg->rows;

        // 1) First, equalize the whole face.
        Mat wholeFace;
        equalizeHist(*faceImg, wholeFace);

        // 2) Equalize the left half and the right half of the face separately.
        int midX = w / 2;
        Mat leftSide = (*faceImg)(Rect(0, 0, midX, h));
        Mat rightSide = (*faceImg)(Rect(midX, 0, w - midX, h));
        equalizeHist(leftSide, leftSide);
        equalizeHist(rightSide, rightSide);

        // 3) Combine the left half and right half and whole face together, so that it has a smooth transition.
        for(int y = 0; y < h; y++)
        {
            for(int x = 0; x < w; x++)
            {
                int v;
                if(x < w / 4)           // Left 25%: just use the left face.
                {
                    v = leftSide.at<uchar>(y, x);
                }
                else if(x < w * 2 / 4)  // Mid-left 25%: blend the left face & whole face.
                {
                    int lv = leftSide.at<uchar>(y, x);
                    int wv = wholeFace.at<uchar>(y, x);
                    // Blend more of the whole face as it moves further right along the face.
                    float f = (x - w * 1 / 4) / (float)(w * 0.25f);
                    v = cvRound((1.0f - f) * lv + (f) * wv);
                }
                else if(x < w * 3 / 4)  // Mid-right 25%: blend the right face & whole face.
                {
                    int rv = rightSide.at<uchar>(y, x - midX);
                    int wv = wholeFace.at<uchar>(y, x);
                    // Blend more of the right-side face as it moves further right along the face.
                    float f = (x - w * 2 / 4) / (float)(w * 0.25f);
                    v = cvRound((1.0f - f) * wv + (f) * rv);
                }
                else                    // Right 25%: just use the right face.
                {
                    v = rightSide.at<uchar>(y, x - midX);
                }
                faceImg->at<uchar>(y, x) = v;
            }// end x loop
        }//end y loop
    }


    static void adjustEyes(Point* leftEyeTl, Point* rightEyeTl)
    {
        Point left(leftEyeTl->x, leftEyeTl->y);
        Point right(rightEyeTl->x, rightEyeTl->y);

        if(left.x <= right.x)
        {
            return;
        }
        else
        {
            rightEyeTl = new Point(left.x, left.y);
            leftEyeTl = new Point(right.x, right.y);
        }
    }

public:
    static Mat PreprocessFaceImage(const Mat& frame, const Rect& faceRect, const Rect& leftEyeRect, const Rect& rightEyeRect)
    {
        Mat theFrame = frame.clone();

        ImageUtils::ConvertToGrayscale(&theFrame);
        equalizeHist(theFrame, theFrame);

        //Apply geometrical transformation to the detected face image to align the eyes
        Mat face = theFrame(faceRect);

        Point left = leftEyeRect.tl();
        Point right = rightEyeRect.tl();
        adjustEyes(&left, &right);

        //Mat warped = FaceDetectionUtils::applyGeometricalTransformation(&face, left, right);
        //do equalization to improve brightness
        equalizeLeftAndRightHalves(&face);
        /*  return warped;*/
        //namedWindow( "Gray image", CV_WINDOW_AUTOSIZE );
        //imshow( "Gray image", face );
        //imwrite("/home/davide/face.png", face);
        theFrame.release();
        return face;
    }
};

#endif // FACEDETECTIONUTILS_H
