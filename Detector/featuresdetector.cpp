#include "featuresdetector.h"

FeaturesDetector::FeaturesDetector()
{
    //Search for many faces.
    flags = CASCADE_SCALE_IMAGE;

    //Smallest face size.
    minFeatureSize = Size(20, 20);

    //How many sizes to search.
    searchScaleFactor = 1.1f;

    // Reliability vs many faces.
    minNeighbors = 4;
}

FeaturesDetector::FeaturesDetector(const QString& name,const  QString& featureFname,const QColor& color)
{
    this->detectorName = QString(name);
    this->featureFileName = QString(featureFname);
    this->color = QColor(color);

    //Search for many faces.
    flags = CASCADE_SCALE_IMAGE;

    //Smallest face size.
    minFeatureSize = Size(20, 20);

    //How many sizes to search.
    searchScaleFactor = 1.1f;

    // Reliability vs many faces.
    minNeighbors = 4;
}

void FeaturesDetector::run()
{
    while(1)
    {
        if(this->frame.empty())
            continue;

        vector<Rect> faces;
        classifier.detectMultiScale(Mat(frame), faces, searchScaleFactor, minNeighbors, flags, minFeatureSize);

        QList<QRect> dd;
        for(const Rect& d : faces)
        {
            dd.append(QRect(QPoint(d.tl().x, d.tl().y), QPoint(d.br().x, d.br().y)));
        }
        emit detected(dd);
        this->msleep(300);
    }
}

void FeaturesDetector::setFrame(const Mat frame)
{
    this->frame = Mat(frame);
}

void FeaturesDetector::LoadClassifier(QString filename)
{
    try
    {
        classifier.load(filename.toStdString().data());
    }
    catch(cv::Exception e)
    {}

    if(classifier.empty())
    {
        cerr << "ERROR: Couldn't load Face Detector (";
        cerr << filename.toStdString().data() << ")!" << endl;
    }
}

QString FeaturesDetector::getName() const
{
    return QString(detectorName);
}

QString FeaturesDetector::getFeaturesFileName() const
{
    return QString(featureFileName);
}

QColor FeaturesDetector::getColor() const
{
    return QColor(color);
}
