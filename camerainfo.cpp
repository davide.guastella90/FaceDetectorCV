#include "camerainfo.h"

CameraInfo::CameraInfo(const QCameraInfo &cameraInfo)
{
    this->cameraInfo = cameraInfo;
}

QList<QSize> CameraInfo::getSupportedResolutions()
{
    return supportedResolution;
}

void CameraInfo::setSupportedResolutions()
{
    QCamera *camera = new QCamera(this->cameraInfo);
    camera->load();
    QCameraImageCapture *imageCapture = new QCameraImageCapture(camera);
    this->supportedResolution = imageCapture->supportedResolutions();

    camera->unload();
    delete imageCapture;
    delete camera;
}


QCameraInfo CameraInfo::getQCameraInfo() const
{
    return QCameraInfo(this->cameraInfo);
}
