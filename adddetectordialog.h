#ifndef ADDDETECTORDIALOG_H
#define ADDDETECTORDIALOG_H

#include <QWidget>
#include <QColorDialog>

namespace Ui {
class AddDetectorDialog;
}

class AddDetectorDialog : public QWidget
{
    Q_OBJECT

public:
    explicit AddDetectorDialog(QWidget *parent = 0);
    ~AddDetectorDialog();

private slots:
    void on_OkButtonClicked();
    void on_CancelButtonClicked();

    void on_featuresColorPushButton_clicked();

signals:
    //(const QString &name, const QString &featureFname, const QColor &color);
    void addDetector(const QString &name, const QString &featureFname, const QColor &color, bool activateNow);

private:
    Ui::AddDetectorDialog *ui;

    QColor color;
};

#endif // ADDDETECTORDIALOG_H
